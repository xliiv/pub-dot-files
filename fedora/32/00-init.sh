#!/bin/bash
set -ex

sudo dnf install --assumeyes \
    clipit \
    cmake \
    curl \
    fish \
    g++ \
    gcc \
    gocryptfs \
    htop \
    make \
    ncdu \
    ranger \
    tig \
    tmux \
    trash-cli \
    tree \
    util-linux-user `# includes chsh command` \
    vim-X11 \
    vim-enhanced \
    which

# install `vim plug`
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

./ff init --dir-path ~/pub-dot-files/
./ff apply --sync-subdir homedir

# install vim plugins & quit
set +e  # vim returns 1 code for my setup - needs better debugging
vim -es -u ~/.vimrc -i NONE -c "PlugInstall" -c "qa" -V
set -e

chsh -s $(which fish) `whoami`
