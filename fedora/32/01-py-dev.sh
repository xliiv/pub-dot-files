#!/bin/sh
set -ex

sudo dnf install --assumeyes \
    python3-devel \
    python3-pip

# https://gist.github.com/yograterol/99c8e123afecc828cb8c
sudo dnf install --assumeyes redhat-rpm-config

sudo pip install \
    python-language-server

sudo pip3 install \
    python-language-server
