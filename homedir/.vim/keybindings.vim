" general vim mappings
" To save, press ctrl-s.
nmap <c-s> :w<CR>
imap <c-s> <Esc>:w<CR>a

" when you forgot to sudo before editing a file..
cmap w!! w !sudo tee % >/dev/null<CR>

" buffers movement
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" vimtabs
map <a-T> :tabnew<cr>
map <a-<> :tabprev<cr>
map <a->> :tabnext<cr>
map ,tn :tabnew %<cr>
map ,tc :tabclose<cr>


"" coping buffer names
" expand to full path
:nmap ,cp :let @+ = expand("%:p")<cr>
" root (one extension removed)
:nmap ,cr :let @+ = expand("%:r")<cr>
" head (last path component removed)
:nmap ,cd :let @+ = expand("%:h")<cr>
" tail (last path component only)
:nmap ,cn :let @+ = expand("%:t")<cr>
" extension only
:nmap ,ce :let @+ = expand("%:e")<cr>

" tabs to spaces, use ":retab"
" map ,t2s :%s/\t/    /gc<CR>
" trim whitespaces
map ,trim :%s/\ \+\n/\r/gc<CR>

" TODO: fix it
" change end of line from u(nix) to d(os)
"map ,u2d :%s///gc<CR>
" change end of line from d(os) to u(nix)
"map ,d2u :%s/ "//gc<CR>

" create session file
map ,mks :mks!
