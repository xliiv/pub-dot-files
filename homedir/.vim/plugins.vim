call plug#begin('~/.vim/plugged')


Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

Plug 'vim-airline/vim-airline'
Plug 'will133/vim-dirdiff'
Plug 'editorconfig/editorconfig-vim'
Plug 'jremmen/vim-ripgrep'
Plug 'dhruvasagar/vim-open-url'
" shows git diff markers in the sign column
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-commentary'
" Defaults everyone can agree on
Plug 'tpope/vim-sensible'

" experimentals
" Plug 'jiangmiao/auto-pairs'
" colors
Plug 'robertmeta/nofrils'
Plug 'romainl/Apprentice'  " dark
Plug 'romainl/Disciple'  " light
Plug 'andreypopp/vim-colors-plain'

" LSP
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'ajh17/vimcompletesme'
Plug 'mattn/vim-lsp-settings'
" bind ctrl+] to go-to
autocmd FileType * setlocal tagfunc=lsp#tagfunc
" extendend completition
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'

Plug 'tpope/vim-fugitive'
map ,gs :tabnew %<cr>:Gstatus<cr>
Plug 'tpope/vim-surround'

" fzf
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" insert file from find
imap <c-x><c-f> <plug>(fzf-complete-path)
" open git tracked file
nmap ,F :GFiles<cr>
" open current dir file
nmap ,f :Files<cr>
" open current dir file
nmap ,b :Buffers<cr>
" snippedts
nmap ,s :Snippets<cr>
" files from current buffer dir
nnoremap <silent> ,5 :Files <C-R>=expand('%:h')<CR><CR>

" Initialize plugin system
call plug#end()


colorscheme nofrils-light
