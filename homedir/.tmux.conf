set-option -g history-limit 10000
set-option -g mouse on
# https://github.com/tmux/tmux/wiki/FAQ#how-do-i-use-a-256-colour-terminal
set-option -g default-terminal "tmux-256color"
# general bindings
set-option -g prefix2 M-[
bind-key -T root M-] command-prompt
bind-key -T root M-/ copy-mode
# window bindings
bind-key -T root M-R command-prompt -I "" "rename-window '%%'"
bind-key -T root M-N new-window -c '#{pane_current_path}'
bind-key -T root M-. next-window
bind-key -T root M-, previous-window
bind-key -T root M-< swap-window -t -1
bind-key -T root M-> swap-window -t +1
bind-key -T root M-C-X confirm-before "kill-window"
# pane bindings
bind-key -T root M-F resize-pane -Z
bind-key -T root M-X confirm-before "kill-pane"
# pane splitting
bind-key -T root M-U split-pane -h -c '#{pane_current_path}'
bind-key -T root M-E split-pane -c '#{pane_current_path}'
# pane selecting
bind-key -T root M-J select-pane -D
bind-key -T root M-K select-pane -U
bind-key -T root M-H select-pane -L
bind-key -T root M-L select-pane -R
# pane selecting by digit
set-option -g base-index 1
set-option -g renumber-windows on
bind-key -n M-1 select-window -t :1
bind-key -n M-2 select-window -t :2
bind-key -n M-3 select-window -t :3
bind-key -n M-4 select-window -t :4
bind-key -n M-5 select-window -t :5
bind-key -n M-6 select-window -t :6
bind-key -n M-7 select-window -t :7
bind-key -n M-8 select-window -t :8
bind-key -n M-9 select-window -t :9
bind-key -n M-0 select-window -t :0
# pane moving
bind-key -T root "M-C-j" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -D; tmux swap-pane -t $old'
bind-key -T root "M-C-k" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -U; tmux swap-pane -t $old'
bind-key -T root "M-C-h" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -L; tmux swap-pane -t $old'
bind-key -T root "M-C-l" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -R; tmux swap-pane -t $old'
# OS clipboard support
set-window-option -g mode-keys vi
bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel "xclip -in -selection clipboard"
bind-key -T copy-mode-vi MouseDragEnd1Pane send -X copy-pipe-and-cancel "xclip -in -selection clipboard"
set-option -g set-clipboard off

# Experimental
set-option -s escape-time 0
set-option -g status-keys vi
set-option -g set-titles on
set-option -g set-titles-string 'tmux - #W'
set-option -g visual-bell off
set-option -g bell-action any
set-window-option -g monitor-activity on
