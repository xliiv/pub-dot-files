# example:
# alias rmi "rm -i"

alias dh='history --delete $history[1]'
alias rl='readlink -f'
alias tp='trash-put'
if type -q vimx
    alias vim=vimx
end
