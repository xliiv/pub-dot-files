# ENVS
# vimx is used (if available) due to alias in alias.fish
set --export EDITOR vim
set --export VISUAL vim


# PATHS
# e.g.: set --global --export PATH <path-to-append> $PATH
set --global --export PATH ~/.local/bin $PATH

# rust
set --global --export PATH ~/.cargo/bin $PATH

# golang
set --global --export PATH /usr/local/go/bin $PATH
mkdir --parents $HOME/go
set --export GOPATH $HOME/go
mkdir --parents $HOME/Android/Sdk
set --export ANDROID_SDK $HOME/Android/Sdk

# Radicle
mkdir --parents $HOME/.radicle/bin
set --global --export PATH $HOME/.radicle/bin $PATH


# ALIASES
source ~/.config/fish/alias.fish


# KEYBINDINGS
function hybrid_bindings --description "Vi-style bindings that inherit emacs-style bindings in all modes"
    for mode in default insert visual
        fish_default_key_bindings -M $mode
    end
    fish_vi_key_bindings --no-erase

    # xliiv's bindings
    # bindings related to functions/
    bind -M insert \co 'ranger-cd; fish_prompt'
    bind -M insert \en 'n; fish_prompt'
    bind -M insert \eo 'lfcd; fish_prompt'
    bind -M insert \e\cu delete-current-history-search
end
set -g fish_key_bindings hybrid_bindings
