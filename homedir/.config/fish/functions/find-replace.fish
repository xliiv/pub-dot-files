function find-replace
    if not count $argv > /dev/null
        echo "Args needed!"
        echo ""
        echo "Type:"
        echo -e "\t" "'find-replace <old-value> <new-value>'"
        echo "to replace <old-value> with <new-value> in current dir and it's descendants"
        return 1
    end

    set SED_CMD "s#$argv[1]#$argv[2]#g"
    rg -l $argv[1] |xargs sed -i $SED_CMD
end
