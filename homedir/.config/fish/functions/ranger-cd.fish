function ranger-cd

  set tempfile '/tmp/chosendir'
  # make ranger display colors correctly in file preview
  # themes http://www.andre-simon.de/doku/highlight/en/theme-samples.php
  # other posibility is
  # https://unix.stackexchange.com/questions/418110/is-there-a-way-to-get-ranger-to-syntax-highlight-files-using-the-x-colorscheme-o
  env HIGHLIGHT_STYLE='fine-blue' /usr/bin/ranger --choosedir=$tempfile (pwd)

  if test -f $tempfile
      if [ (cat $tempfile) != (pwd) ]
        cd (cat $tempfile)
      end
  end

  rm -f $tempfile

end
