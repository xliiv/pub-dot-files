function git-squash
    if not [ (count $argv) -eq 1 ]
        echo "Args needed!"
        echo ""
        echo "Type:"
        echo -e "\t" "'git-squash <upstream-branch>'"
        echo ""
        echo "to merge (with --squash) current-branch and <upstream-branch>"
        echo ""
        return 1
    end

    set UPSTREAM_BRANCH $argv[1]
    set FEATURE_BRANCH (git rev-parse --abbrev-ref HEAD)
    set SQUASHED_BRANCH (git rev-parse --abbrev-ref HEAD)-squashed

    if [ (count $argv) -eq 1 ]
        git co $UPSTREAM_BRANCH
        git co -b $SQUASHED_BRANCH
        git merge --squash $FEATURE_BRANCH
    end
end
