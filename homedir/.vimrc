" xliiv's general settings
set shell=/bin/bash
set nocompatible
syntax enable
filetype plugin on

" clipboard, coping, pasting
" 'set paste' note: paste breaks snippets in terminal vim
nnoremap ,tp :set invpaste paste?<CR>
imap ,tp <C-O>:set invpaste paste?<CR>
" make yanking & pasting using system clipboard, requires:+xterm_clipboard
set clipboard=unnamedplus

set encoding=utf-8 " Use UTF-8.
" info, look, accessability
" colorscheme is set ~/.vim/plugins.vim because it depends on Plug
set visualbell " Error bells are displayed visually.
set lazyredraw " redraw only when we need to. (boost for macros execution)
set number " line numbering
set ruler " Show line number, cursor position.
set cursorline " highlight current line
set showmatch           " highlight matching for braces, parenthesis, etc.
set showcmd " Display incomplete commands.(airline plugin makes it hidden)
set scrolloff=999 " Minimal number of screen lines to keep above and below the cursor.
set wildmenu " Show autocomplete menus.
set showmode " Show editing mode
set laststatus=2 " display always status line, eg for airline plugin
" make whitespace visible
set list
set listchars=tab:>.,trail:.,extends:#,nbsp:.
autocmd BufNewFile,BufRead *.taskpaper set nolist
set colorcolumn=78
" searching
set wildignorecase " ignorecase tab completition
set path+=** " walk in recursively when complete file names
set incsearch " Search as you type.
set ignorecase " Ignore case when searching.
set hlsearch " Highlight matched.
" tabs and spaces
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
au BufNewFile, BufRead, *.html set tabstop=2
au BufNewFile, BufRead, *.html set shiftwidth=2
" line wrapping
set nofixendofline " keep end of line as is (prevents new line putting)
set wrap
" folding
set foldenable          " enable folding
set foldlevelstart=10   " open most folds by default, 0 all collapsed, 99 all expanded
set foldnestmax=10      " 10 nested fold max
set foldmethod=indent   " fold based on indent level
nnoremap <space> za     " folding by space
" netrw
let g:netrw_altv=1

source ~/.vim/plugins.vim
source ~/.vim/keybindings.vim


" EXPERIMENTS:
"Exuberant Ctags
"TODO: decide whether to use of Exuberant Ctags
"command! MakeTags !ctags -R .
" ] goto
" t go back
" g] ambiguous tags
