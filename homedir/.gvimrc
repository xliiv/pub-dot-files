" Don't display the menu or toolbar. Just the screen.
" set guioptions-=m
set guioptions-=T
set guifont=Monospace\ 9

" hide tab bar
set showtabline=1

" Set color scheme that I like.
if has("gui_running")
    colorscheme nofrils-light
endif
