#!/bin/bash

sudo apt-get update
sudo apt-get install --yes \
    clipit \
    curl \
    fish \
    gcc \
    gocryptfs \
    htop \
    ncdu \
    net-tools \
    nnn \
    ranger \
    ripgrep \
    tig \
    tmux \
    trash-cli \
    tree

# better clipboard support over regular vim
sudo apt-get install -y vim-gtk

# install `vim plug`
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

./ff init --dir-path ~/pub-dot-files/
./ff apply --sync-subdir homedir

# install vim plugins & quit
set +e  # vim returns 1 code for my setup - needs better debugging
vim -es -u ~/.vimrc -i NONE -c "PlugInstall" -c "qa" -V
set -e

chsh -s $(which fish) `whoami`
