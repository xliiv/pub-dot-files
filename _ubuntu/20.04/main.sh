#!/usr/bin/env bash

set -ex

DIR_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

$DIR_PATH/00-init.sh
$DIR_PATH/01-py-dev.sh
