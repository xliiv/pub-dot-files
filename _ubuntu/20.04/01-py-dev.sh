#!/bin/sh

set -e
sudo apt-get install --yes \
    python3-pip

sudo pip3 install \
    python-language-server
