#!/bin/bash

sudo apt-get update
sudo apt-get install -y \
    clipit \
    curl \
    fish \
    gcc \
    htop \
    ncdu \
    ranger \
    tig \
    tmux \
    trash-cli \
    tree

# better clipboard support over regular vim
sudo apt-get install -y vim-gtk

# install `vim plug`
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

chsh -s /usr/bin/fish `whoami`
